
import { html,css,LitElement } from "lit";

export class Greeting extends LitElement {
    static styles = css`p { color: red }`;

    static properties = {
        name: {type: String}
    };

    constructor() {
        super();
        this.name = 'Somebody';
    }

    render() {
        return html`<p>Hello ${this.name}!</p>`;
    }
}

customElements.define('rhit-greeting', Greeting);